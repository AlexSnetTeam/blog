from asfblog.default_settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '/tmp/db.sqlite3',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# ################################################
#           Analytics
# ################################################

ANALYTICAL_AUTO_IDENTIFY = False
ANALYTICAL_INTERNAL_IPS = ['127.0.0.1','192.168.0.1',]

# Chartbeat
CHARTBEAT_USER_ID = ''

# Clicky
CLICKY_SITE_ID = ''

# Crazy Egg
CRAZY_EGG_ACCOUNT_NUMBER = ''

#Gaug.es
GAUGES_SITE_ID = ''

# Google Analytics
GOOGLE_ANALYTICS_PROPERTY_ID = ''
GOOGLE_ANALYTICS_SITE_SPEED = True

# HubSpot
HUBSPOT_PORTAL_ID = ''
HUBSPOT_DOMAIN = ''

# KISSinsights
KISS_INSIGHTS_ACCOUNT_NUMBER = ''
KISS_INSIGHTS_SITE_CODE = ''

# KISSmetrics
KISS_METRICS_API_KEY = ''

# Mixpanel
MIXPANEL_API_TOKEN = ''

# Olark
OLARK_SITE_ID = ''

# Optimizely
OPTIMIZELY_ACCOUNT_NUMBER = ''

# Performable
PERFORMABLE_API_KEY = ''

# Reinvigorate
REINVIGORATE_TRACKING_ID = ''

# Woopra
WOOPRA_DOMAIN = ''

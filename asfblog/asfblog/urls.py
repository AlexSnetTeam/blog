from django.conf.urls import patterns, include, url
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# Sitemaps
from django.contrib.sitemaps import FlatPageSitemap, GenericSitemap
from asfblog.apps.posts.models import Post
info_dict = {
    'queryset': Post.publisged_posts(),
    'date_field': 'publish_date',
    }

sitemaps = {
    #'flatpages': FlatPageSitemap,
    'blog': GenericSitemap(info_dict, priority=0.6),
    }

urlpatterns = patterns('',

    # Main page
    url('', include('asfblog.apps.mainpage.urls')),

    # Posts
    url('', include('asfblog.apps.posts.urls', namespace='posts')),

    # Admin panel
    url(r'^admin/', include(admin.site.urls)),

    # Sitemap
    (r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),


)

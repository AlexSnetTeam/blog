from django.conf.urls.defaults import patterns, url
from asfblog.apps.mainpage.views import MainView, AboutView

urlpatterns = patterns('',
    url(r'^$', MainView.as_view(), name='mainpage'),
    url(r'^about.html$', AboutView.as_view(), name='about'),
)
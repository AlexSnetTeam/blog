jQuery(document).ready(function() {
    jQuery("abbr.timeago").timeago();

});

function replaceURLWithHTMLLinks(text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(exp,"<a href='$1'>$1</a>");
}

twitter_timeline = function(data){
    jQuery.each(data, function(index, value){
        jQuery("#twitter ul").append("<li>"+replaceURLWithHTMLLinks(value['text'])+"</li>");
    });
};
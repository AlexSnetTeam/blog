import os, sys

from django.utils.translation import ugettext_lazy as _, ugettext as __
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, RedirectView
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.utils import simplejson
from django.conf import settings


class AboutView(TemplateView):
    template_name = "mainpage/about.html"

class MainView(RedirectView):
    url = '/posts.html'


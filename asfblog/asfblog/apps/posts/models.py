from django.contrib.auth.models import User
from django.utils.translation import ugettext as __, ugettext_lazy as _
from django.contrib.auth.management import create_superuser
from django.contrib.auth import models as auth_models
from django.contrib.admin import SimpleListFilter
from django.db.models import Q
from django.contrib import admin
from django.contrib.sitemaps import ping_google
from django.contrib.markup.templatetags import markup
from django.db import models

import datetime

class Tag(models.Model):
    tag = models.CharField(max_length=32, verbose_name=_('tag'))

class Post(models.Model):
    title = models.CharField(max_length=255, verbose_name=_('title'))
    slug = models.SlugField(max_length=255, verbose_name=_('slug'), db_index=True)

    cover = models.ImageField(upload_to='covers', verbose_name=_('cover'), null=True, blank=True)

    create_date = models.DateTimeField(auto_created=True, auto_now_add=True)
    modify_date = models.DateTimeField(auto_now=True)
    publish_date= models.DateTimeField(verbose_name=_('publish date'),null=True, blank=True, help_text=_('if is set then post marked as published'))

    preview_source = models.TextField(verbose_name=_('preview source'))
    preview_render = models.TextField(verbose_name=_('rendered preview'), editable=False, null=True, blank=True)

    text_source = models.TextField(verbose_name=_('page source'))
    text_render = models.TextField(verbose_name=_('rendered page'), editable=False, null=True, blank=True)

    @classmethod
    def publisged_posts(cls):
        return cls.objects.exclude(Q(publish_date__isnull=True))

    def save(self, force_insert=False, force_update=False, using=None):
        self.preview_render = markup.markdown(self.preview_source) if self.preview_source else None
        self.text_render = markup.markdown(self.text_source) if self.text_source else None
        try:
            ping_google()
        except Exception:
            pass
        super(Post, self).save(force_insert=force_insert, force_update=force_update, using=using)

    @models.permalink
    def get_absolute_url(self):
        return (
            'posts:details',
            (),
            {
                'slug' : self.slug
            }
        )
    get_absolute_url.short_description = _('post url')

    class Admin(admin.ModelAdmin):

        class PublishedFilter(SimpleListFilter):
            title = _('is published')

            # Parameter for the filter that will be used in the URL query.
            parameter_name = 'published'

            def lookups(self, request, model_admin):
                return (
                    ('n', _('not published')),
                    ('y', _('published')),
                )

            def queryset(self, request, queryset):
                if self.value() == 'n':
                    return queryset.filter(Q(publish_date__isnull=True))
                if self.value() == 'y':
                    return queryset.exclude(Q(publish_date__isnull=True))


        def published(self, obj):
            if obj.publish_date:
                return True
            return False


        list_display = (
            'title',
            'get_absolute_url',
            'published'
        )

        list_filter = [
            PublishedFilter,
        ]


admin.site.register(Post, Post.Admin)
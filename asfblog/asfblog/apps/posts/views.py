import os, sys

from django.utils.translation import ugettext_lazy as _, ugettext as __
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from django.http import HttpResponse
from django.utils import simplejson
from django.conf import settings

from asfblog.apps.posts.models import Post, Tag


class DetailsView(TemplateView):
    template_name = "posts/details.html"

    def get_context_data(self, **kwargs):
        resp = super(DetailsView,self).get_context_data(**kwargs)
        resp.update({
            'post' : Post.objects.get(slug=kwargs['slug'])
        })
        return resp

class ListView(TemplateView):
    template_name = "posts/list.html"

    def get_context_data(self, **kwargs):
        resp = super(ListView,self).get_context_data(**kwargs)
        resp.update({
            'posts' : Post.objects.exclude(publish_date=None).order_by('-publish_date')[:10]
        })
        return resp

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)
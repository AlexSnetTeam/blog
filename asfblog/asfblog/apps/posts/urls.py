from django.conf.urls.defaults import patterns, url

from asfblog.apps.posts.views import DetailsView, ListView

urlpatterns = patterns('',
    url(r'^posts.html$', ListView.as_view(), name='list_index', kwargs={'page':1}),

    url(r'^(?P<slug>\w+\s*\S+).html$', DetailsView.as_view(), name='details'),

    url(r'^page/(?P<page>\d+).html$', ListView.as_view(), name='list'),
    url(r'^page/(?P<page>\d+).json$', ListView.as_view(), name='list_json', kwargs={'json':True}),
)
